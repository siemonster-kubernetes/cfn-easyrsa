from certauth.certauth import CertificateAuthority
from certauth.cache import SsmCache


store = SsmCache('./certs')
ca = CertificateAuthority('My Custom CA', 'my-ca.pem', cert_cache=store)
filename = ca.cert_for_host('example.com')