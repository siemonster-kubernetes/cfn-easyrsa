#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

docker run --rm -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:build-python3.8
