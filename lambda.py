#!/usr/bin/env python
from crhelper import CfnResource
import boto3
import logging


logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')


@helper.create
def create(event, context):
    logger.info("Got Create")
    if event['ResourceType'] == "Custom::Ca":
        pass
        # create CA

    if event['ResourceType'] == "Custom::Certificate":
        pass
        # create Certificate


@helper.delete
def delete(event, context):
    logger.info("Got Delete")


def handler(event, context):
    helper(event, context)
